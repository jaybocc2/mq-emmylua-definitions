--- @class array
--- @deprecated
--- @field public Dimensions number Number of dimensions in the array
--- @field public Size number Total number of elements in the array
--- @field public ToString string None

